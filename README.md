# Create a Collatz Generator
## C# version
The point here would be to create some great graphs using e.graphics and have some computer power, maybe use parralell processing to create the data and then display the final graph with a slider of some sort and let the user then import his color palette and create his perfect 4k design to download in png.

## Web Version
The web app could be super interesting to have multiple math generators like this and let people mess around with size, precision, resolution, colors, orientation etc... and then share their creation with the rest of the users. So you could be either:

playing with the generator for fun
playing with the generator to create this lit background image that you always dreamed of
Just looking around for great and satisfying images and maybe take one to use in background
It coud be INCREDIBLE to have like a gif generator that gradually creates the graph like a plant growing

### inspiration
https://mathematica.stackexchange.com/questions/85718/trying-to-visualize-the-collatz-conjecture

<img src="./mdImg/collatz.png">

## Create a color grading tool
With this C# or web tool you woud just input 2 hexadecimal values and it replies you with some interpolated the real point would be to create some color palettes, like you need 6 colors to get from one to an other, bam the site gives you 6 tiles gradually fading. The algorythm of that could be used in all the math graphing tools that i could create and make them look amazing

### inspiration
The Color grading on the collatz example