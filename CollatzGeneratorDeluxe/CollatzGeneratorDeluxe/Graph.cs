﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollatzGeneratorDeluxe
{
    internal class Graph
    {
        const int DEFAULT_HEIGHT = 100;
        const int DEFAULT_WIDTH = 100;
        const int IGNORING_PERCENTAGE = 10;

        Image _img;
        Size _size;
        List<int>[] _data;

        public Image Img { get => _img; set => _img = value; }
        public Size Size { get => _size; set => _size = value; }
        public List<int>[] Data { get => _data; set => _data = value; }

        public Graph(List<int>[] data) : this(new Bitmap(DEFAULT_WIDTH, DEFAULT_HEIGHT), new Size(DEFAULT_WIDTH, DEFAULT_HEIGHT), data)
        {

        }
        public Graph(Image img, Size size, List<int>[] data)
        {
            Img = img;
            Size = size;
            Data = data;
        }

        private int GetMaxValue()
        {
            return Data.Length;
        }
        private int GetMaxResult()
        {
            int max = 0;
            foreach (List<int> results in Data)
            {
                if (results != null)
                {
                    foreach (int value in results)
                    {
                        if (value > max)
                        {
                            max = value;
                        }
                    }
                }
            }
            return max;
        }

        public int[] CalculateOccurences()
        {
            int[] occurences = new int[GetMaxResult() + 1];
            foreach (List<int> results in Data)
            {
                if (results != null)
                {
                    foreach (int value in results)
                    {
                        occurences[value]++;
                    }
                }
            }
            return occurences;
        }

        public List<int>[] Interpolate()
        {
            List<int>[] interpolatedData = (List<int>[])Data.Clone();
            int counter = 0;
            foreach (List<int> results in interpolatedData) 
            {
                if (results != null)
                {
                    //interpolatedData[counter++] = results;
                    List<int> newResults = new List<int>();
                    int counter2 = 0;
                    foreach (int value in results)
                    {
                        newResults.Add(value);
                        if (counter2 <= results.Count - 1)
                        {
                            newResults.Add(value + ((results[counter2] - value) / 2));
                        }
                        counter2++;
                    }

                    interpolatedData[counter++] = newResults;
                }
            }

            return interpolatedData;
        }
        public Image DrawPathsColored(Brush backgroundColor)
        {
            Img = new Bitmap(this.Size.Width, this.Size.Height);
            using (Graphics g = Graphics.FromImage(Img))
            {
                g.FillRectangle(backgroundColor, new Rectangle(new Point(0, 0), Size));
            }

            double xMultiplier = (Size.Width / (double)GetMaxValue());
            double yMultiplier = (Size.Height / (double)GetMaxResult());

            int X;
            int Y;

            List<int>[] inter = Interpolate();
            foreach (List<int> results in inter)
            //foreach (List<int> results in Data)
            {
                int[] occurences = CalculateOccurences();
                if (results != null)
                {
                    int nbrResults = results.Count;
                    Point prev = new Point(0, Size.Height);
                    Point act = new Point(0, Size.Width);
                    for (int i = 0; i < nbrResults; i++)
                    {
                        int value = results[i];


                        X = (int)((i + 1) * xMultiplier);
                        Y = Size.Height - (int)(value * yMultiplier);

                        prev = new Point(act.X, act.Y);
                        act = new Point(X, Y);

                        using (Graphics g = Graphics.FromImage(Img))
                        {
                            int red = (occurences[i] * 4) % 255;
                            int green = (occurences[i] * 4) % 255;
                            int blue = (occurences[i] * 4) % 255;
                            Color pointColor = Color.FromArgb(red, green, blue);
                            g.DrawLine(new Pen(pointColor,1), prev, act);
                        }
                    }
                }
            }

            return Img;
        }
        public Image DrawPaths(Brush backgroundColor, Pen graphColor)
        {
            Img = new Bitmap(this.Size.Width, this.Size.Height);
            using (Graphics g = Graphics.FromImage(Img))
            {
                g.FillRectangle(backgroundColor, new Rectangle(new Point(0, 0), Size));
            }

            double xMultiplier = (Size.Width / (double)GetMaxValue());
            double yMultiplier = (Size.Height / (double)GetMaxResult());

            int X;
            int Y;

            foreach (List<int> results in Data)
            {
                if (results != null)
                {
                    int nbrResults = results.Count;
                    Point prev = new Point(0, Size.Height);
                    Point act = new Point(0, Size.Width);
                    for (int i = 0; i < nbrResults; i++)
                    {
                        int value = results[i];
                        

                        X = (int)((i+1) * xMultiplier);
                        Y = Size.Height-(int)(value * yMultiplier);

                        prev = new Point(act.X,act.Y);
                        act = new Point(X, Y);

                        using (Graphics g = Graphics.FromImage(Img))
                        {
                            g.DrawLine(graphColor, prev, act);
                        }
                    }
                }
            }

            return Img;
        }
        public Image DrawMaxValues(Brush backgroundColor, Brush graphColor)
        {
            Img = new Bitmap(this.Size.Width, this.Size.Height);
            using (Graphics g = Graphics.FromImage(Img))
            {
                g.FillRectangle(backgroundColor, new Rectangle(new Point(0, 0), Size));
            }

            List<int> maxs = new List<int>();

            //collects all the max values 
            foreach (List<int> results in Data)
            {
                if (results != null)
                {
                    int max = 0;
                    foreach (int value in results)
                    {
                        if (value > max)
                        {
                            max = value;
                        }
                    }
                    maxs.Add(max);
                }
            }
            //little correction to remove the 10% highest values to make a better graph
            int maxTestValue = GetMaxValue();
            int maxResult = GetMaxResult();

            //this is in an attempt to have a better looking graph
            maxResult /= 4;

            double pixelToValueHeight = (Size.Height / (maxTestValue * 1.0));
            double pixelNbrForOneUnitWidth = (Size.Width / (maxResult * 1.0));
            int X;
            int Y;

            int yOffset = 0;

            foreach (int max in maxs)
            {
                //on the X axis we have the max value when tested
                //on the Y axis we have the starting values
                yOffset++;

                Y = (int)(Size.Height - (max * pixelNbrForOneUnitWidth));
                X = (int)((yOffset * pixelToValueHeight));
                //just to make things a little more visible
                X -= 5;
                Y -= 5;
                using (Graphics g = Graphics.FromImage(Img))
                {
                    g.FillEllipse(graphColor, new Rectangle(new Point(X, Y), new Size(10, 10)));
                }
            }

            return Img;
        }
    }
}
