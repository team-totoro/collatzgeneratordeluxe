﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CollatzGeneratorDeluxe
{
    public partial class Form1 : Form
    {
        CollatzGenerator CG;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            CG = new CollatzGenerator(1,100);
            CG.Generate();

            Graph gra = new Graph(pbx1.Image,pbx1.Size,CG.Results);

            pbx1.Image = gra.DrawPathsColored(Brushes.Black);
            //pbx1.Image = gra.DrawPaths(Brushes.Black,new Pen(Brushes.White,1));

            pbx1.Size = pbx1.Size;

        }
    }
}
