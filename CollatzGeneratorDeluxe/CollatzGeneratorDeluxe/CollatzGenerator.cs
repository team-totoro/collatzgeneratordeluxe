﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollatzGeneratorDeluxe
{
    internal class CollatzGenerator
    {
        const int DEFAULT_START_VALUE = 1;
        const int DEFAULT_END_VALUE = 10;
        const int MINIMAL_COLLATZ_VALUE = 1;
        //this value could be 4,2 or 1
        const int COLLATZ_LOOP_VALUE = 1;

        int _startValue;
        int _endValue;
        //This list could be interpreted as a weird and unnescessary thing
        //But it could get really interesting if one day we want to add the abilty for the user to only input a certain set of input numbers
        List<int> _valuesTocheck;
        //this array of list is made like that because we know how much data we have but we dont know how much step every one is gonna take
        List<int>[] _results;

        public int StartValue
        {
            get { return _startValue; }
            set
            {
                // as the Collatz Generator only works with POSITIVE numbers we are required to check if they are
                if (value < MINIMAL_COLLATZ_VALUE)
                {
                    value = MINIMAL_COLLATZ_VALUE;
                }
                _startValue = value;
            }
        }

        public int EndValue
        {
            get { return _endValue; }
            set
            {
                // same reason as for the start value
                if (value < MINIMAL_COLLATZ_VALUE)
                {
                    value = MINIMAL_COLLATZ_VALUE;
                }
                //if the end value is inferior to start value its not possible so we switch the two
                //this means that we need to set the start value BEFORE the EndValue
                if (StartValue > value)
                {
                    int tmp = StartValue;
                    StartValue = value;
                    value = tmp;
                }
                _endValue = value;
            }
        }
        public List<int> ValuesToCheck { get => _valuesTocheck; set => _valuesTocheck = value; }
        public List<int>[] Results { get => _results; set => _results = value; }

        public CollatzGenerator() : this(DEFAULT_START_VALUE, DEFAULT_END_VALUE)
        {
            //we have a default generator with default values for easy testing
        }
        public CollatzGenerator(int startValue, int endValue)
        {
            StartValue = startValue;
            EndValue = endValue;
            ValuesToCheck = new List<int>();
            for (int i = startValue; i <= endValue; i++)
            {
                ValuesToCheck.Add(i);
            }

            Results = new List<int>[ValuesToCheck.Count];
        }
        public CollatzGenerator(List<int> valuesToCheck)
        {
            //in case sometime we decide to allow the user to input a very specific data set but its not yet fully supported
            foreach (int value in valuesToCheck)
            {
                //even if in logic the user should be intelligent if he uses this method, to not have a too heavy code on the generation we do the check upfront
                if (value < MINIMAL_COLLATZ_VALUE)
                {
                    valuesToCheck[value] = MINIMAL_COLLATZ_VALUE;
                }
            }

            ValuesToCheck = valuesToCheck;

            Results = new List<int>[ValuesToCheck.Count];
        }

        public void Generate()
        {
            int listLength = ValuesToCheck.Count;
            int cursor = 0;
            if (ValuesToCheck[0] == COLLATZ_LOOP_VALUE)
            {
                cursor++;
            }
            int theStartValue = ValuesToCheck[cursor];
            

            //init of the array
            for (int i = theStartValue; i < listLength; i++)
            {
                Results[i] = new List<int>();
            }

            Parallel.For(theStartValue, listLength, i =>
            //for (int i = theStartValue ;i < listLength;i++)
            {
                //we set the array
                int currentResult = i;
                Results[i].Add(currentResult);
                while (currentResult != COLLATZ_LOOP_VALUE)
                {
                    //the collatz conjectur says that if you apply those 2 rules it will everytime eventually come back to 1
                    // 1 : if the number is even divide it by 2
                    // 2 : if the number is odd multiply it by 3 and add 1
                    if (currentResult % 2 == 0)
                    {
                        //the number is even
                        currentResult = currentResult / 2;
                    }
                    else
                    {
                        //the number is odd
                        currentResult = (currentResult * 3) + 1;
                    }
                    Results[i].Add(currentResult);
                }
            });
        }
    }
}
